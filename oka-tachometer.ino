/*
   Tachometer for car VAZ 11113 "OKA"
*/
#ifndef FDB_LIQUID_CRYSTAL_I2C_H
  #include <LiquidCrystal_I2C.h>
#endif

LiquidCrystal_I2C lcd(0x27,16,2); // A4,A5

const PROGMEM char sHello[] = {'H','e','l','l','o','!','\0'}; // Hello

#define IGN_ON HIGH // зажечь искру
#define IGN_OFF LOW // потушить искру

#define HALL 8 // вход датчика Холла
#define COMM 2 // выход на коммутатор

#define TIMEOUT 200 // 1/5 sec or 5 Hz
unsigned long last_micros;
unsigned long last_millis;
unsigned long count_freq;
bool flag;
//======================================================================
void setup() {
  pinMode(HALL, INPUT); // вход датчика Холла
  pinMode(COMM, OUTPUT); // выход управления коммутатором
  digitalWrite(COMM, LOW); // потушить искру

  // инициализируем дисплей и загружаем образы русских букв
  lcd.begin();
  lcd.backlight();
  lcd.home();
  lcd.setCursor(0,0);
  lcdPrintProgmem(sHello);

  count_freq = 0;
  flag = IGN_OFF;
  last_millis = millis(); // для дисплея
  last_micros = micros(); // должно стоять в самом конце
}
//======================================================================
void loop() {
  unsigned long cm;
  if (digitalRead(HALL)==IGN_ON) {
    // начало искры на ДХ
    if (flag==IGN_OFF) {
      flag = IGN_ON;
      // сколько микросекунд прошло с предыдущей искры
      cm = micros();
      count_freq = iter(cm, last_micros);
      last_micros = cm;
    }
  } else {
    if (flag==IGN_ON) flag = IGN_OFF;
  }
  // по таймауту обновляем показания дисплея
  if (iter(millis(), last_millis)>TIMEOUT) {
    last_millis = millis();
    lcd.setCursor(0,1);
// === частота в RPM ===
//    if (count_freq != 0) lcd.print(60000000/count_freq);
//    lcd.print("rpm");
// === частота в Гц ===
    if (count_freq != 0) lcd.print(1000000/count_freq);
    lcd.print("Hz");
  }
}
//======================================================================
void lcdPrintProgmem(const char* s) {
  for (int i = 0; i < strlen_P(s); i++) lcd.write(pgm_read_byte_near(s + i));
}
//======================================================================
// вычисляем правильный интервал для millis и micros с учётом переполнения
unsigned long iter(unsigned long curr, unsigned long prev) {
  if (curr<prev) return ++curr+~prev; else return curr-prev;
}
